_______________________________________________________________________________
# Corpus TeMario (TExtos com sumMÁRIOs)
#### Online resources for Automatic Text Summarization in Portuguese
_______________________________________________________________________________

### Authors of this repo:
  * Jonathan Rojas Simón (IDS_Jonathan_Rojas@hotmail.com)
  * Yulia Ledeneva (yledeneva@yahoo.com)
  * René Arnulfo García-Hernández (renearnulfo@hotmail.com)

_______________________________________________________________________________

### Authors of corpus:
  * Thiago Alexandre Salgueiro Pardo
  * Lucia Helena Machado Rino

_______________________________________________________________________________

#### Overview
_______________________________________________________________________________
TeMario is a set of 100 newspaper articles written in Brazilian Portuguese  
destined for research purposes (e.g. Automatic Text Summarization, Evaluation  
of Text  Summaries, Linguistic Analysis, etc.). In their composition, TeMario  
was constructed from two main journals of Brazil ("Folha de São Paulo" and   
"Journal do Brasil"). 

All articles are distributed into five sections, these one are the following:  
  * Special
  * World 
  * Opinion
  * International
  * Politics

For each document of TeMario, a gold-standard summary generated from a  
professional writer was included. These summaries are useful to evaluate the  
quality of automatic summaries with ROUGE system.  
_______________________________________________________________________________

#### Contents of this repo
_______________________________________________________________________________
Due to the lack of explicit determination of elements (sentences, paragraphs  
and dates) from original sources, several segmentations of elements were   
performed. In the paper "Calculating the Upper Bounds for Automatic Text   
Summarization in Portuguese using Genetic Algorithms" (see [URL-PAPER]),  
we found that the use of differents segmentations of elements affects the   
performance for any method and heuristic. In consecuence, we perform some  
segmentations to calculate the upper bounds for Automatic Text Summarization  
in Portuguese, these one include the following criteria:  
  * By paragraphs
  * By human segmentation of sentences
  * By automatic segmentation of sentences tool (see [SENTER])

The contents of this repo are the following:
  * In "0000__TeMario_origin" you will find the original resources of TeMario  
    corpus retrieved from [linguateca.pt].
  * In "0001__TeMario_Tagged" you will find the labeled documents performed  
    by humans.
  * In "0002__TeMario_Sentences-splitted" you will find the documents without  
    any label. However, the content of each document is divided into   
    paragraphs or sentences per line.
  * In "0003__Gold-Standard_Summaries" you will find all gold-standard   
    summaries described above.  

_______________________________________________________________________________

#### Citation
_______________________________________________________________________________

If you use these documents, please cite it with:


  
 * (Rojas et al., 2018) - Rojas-Simón J., Ledeneva Y., García-Hernández R.A. 
                       (2018) Calculating the Upper Bounds for Portuguese Automatic 
                       Text Summarization Using Genetic Algorithm. In: Simari G., 
                       Fermé E., Gutiérrez Segura F., Rodríguez Melquiades J. (eds) 
                       Advances in Artificial Intelligence - IBERAMIA 2018. IBERAMIA 
                       2018. Lecture Notes in Computer Science, vol 11238. Springer, Cham
 * (Pardo & Rino, 2003) - Pardo, T., Rino, L.: TeMário: Um Corpus para 
                       Sumarização Automática de Textos, NILC - ICMC-USP, São 
                       Carlos, Brasil, (2003).

  [URL-PAPER]: <https://link.springer.com/chapter/10.1007/978-3-030-03928-8_36>
  [linguateca.pt]: <https://www.linguateca.pt/Repositorio/TeMario/>
  [SENTER]: <http://conteudo.icmc.usp.br/pessoas/taspardo/SENTER_Por.zip>
