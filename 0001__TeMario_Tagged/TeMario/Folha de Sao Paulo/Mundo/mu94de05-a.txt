<DOC>

<FILEID>
<s docid="mu94de05-a" num="1" wdcount="1">mu94de05-a</s>
</FILEID>

<HEAD>
<s docid="mu94de05-a" num="2" wdcount="6"> Virada nos EUA prejudica Am�rica Latina </s>
<s docid="mu94de05-a" num="3" wdcount="4"> Tradu��o de Clara Allain </s>
<s docid="mu94de05-a" num="4" wdcount="4"> Especial para a Folha </s>
</HEAD>

<AUTHOR>
<s docid="mu94de05-a" num="5" wdcount="2"> JORGE CASTA�EDA </s>
</AUTHOR>

<CATEGORY>
<s docid="mu94de05-a" num="6" wdcount="1"> Mundo </s>
</CATEGORY>



<TEXT>


<p docid="mu94de05-a" nump="1" wdcount="85" nums="3">
<s docid="mu94de05-a" num="7" wdcount="68">Pelo efeito sobre a margem de manobra e a capacidade de negocia��o do presidente Bill Clinton, pelo impacto ideol�gico da vit�ria avassaladora da direita republicana e pelas repercuss�es do  sim dado na Calif�rnia � Proposta 187, relativa aos direitos dos estrangeiros n�o-documentados no pa�s, as elei��es norte-americanas de 8 de novembro podem transformar-se numa das mais significativas em muitos anos para o M�xico e para a Am�rica Latina.</s>
<s docid="mu94de05-a" num="8" wdcount="17">Elas correm o risco de anular as oportunidades abertas pela chegada ao poder dos democratas, em 1992.</s>
</p>
<p docid="mu94de05-a" nump="2" wdcount="57" nums="3">
<s docid="mu94de05-a" num="9" wdcount="11">Clinton � hoje um presidente ferido �n�o de morte, mas seriamente.</s>
<s docid="mu94de05-a" num="10" wdcount="26">Ele vai enfrentar dificuldades insuper�veis no campo da pol�tica nacional, sobretudo no que diz respeito � aprova��o de uma reforma de fundo do sistema de sa�de.</s>
<s docid="mu94de05-a" num="11" wdcount="20">Talvez ele consiga deter a ofensiva conservadora dos republicanos rec�m-eleitos, mas sua pr�pria agenda ser�, sem d�vida, congelada at� 1996.</s>
</p>
<p docid="mu94de05-a" nump="3" wdcount="73" nums="2">
<s docid="mu94de05-a" num="12" wdcount="14">Na �rea da pol�tica externa provavelmente acontecer� a mesma coisa, embora em menor grau.</s>
<s docid="mu94de05-a" num="13" wdcount="59">Os novos dirigentes republicanos da C�mara e do Senado, de Jesse Helms a Newt Gingrich, j� manifestaram sua inten��o de imprimir uma nova orienta��o � pol�tica externa norte-americana, em �mbitos t�o diferentes quanto o Gatt, a assist�ncia a outros pa�ses, a participa��o em miss�es sob o comando da ONU e o envio de tropas ao Haiti e outros pa�ses.</s>
</p>
<p docid="mu94de05-a" nump="4" wdcount="39" nums="1">
<s docid="mu94de05-a" num="14" wdcount="13">Nessas condi��es, dificilmente ser�o concretizadas as esperan�as dos presidentes latino-americanos convocados a Miami por Clinton, de ver realizados seus sonhos de uma grande zona hispano-americana de livre com�rcio.</s>
</p>
<p docid="mu94de05-a" nump="5" wdcount="39" nums="1">
<s docid="mu94de05-a" num="15" wdcount="13">Elei��es </s>
</p>
<p docid="mu94de05-a" nump="6" wdcount="70" nums="3">
<s docid="mu94de05-a" num="16" wdcount="16">Clinton � hoje um dirigente cuja reelei��o �obsess�o �nica de todo presidente norte-americano� est� em risco.</s>
<s docid="mu94de05-a" num="17" wdcount="25">Se antes ele se via obrigado a subordinar alguns aspectos da pol�tica externa �s quest�es da vida interna norte-americana, agora tal inclina��o tender� a exacerbar-se.</s>
<s docid="mu94de05-a" num="18" wdcount="29">Tudo se sujeitar� aos imperativos da campanha de 1996: Cuba, Haiti, a quest�o dos imigrantes, o livre com�rcio, o combate ao narcotr�fico, a devolu��o do Canal do Panam� etc.</s>
</p>
<p docid="mu94de05-a" nump="7" wdcount="39" nums="1">
<s docid="mu94de05-a" num="19" wdcount="13">Tendo em vista que os opositores mais perigosos de Clinton ser�o, entre outros, o governador da Calif�rnia, Pete Wilson, que assegurou sua reelei��o gra�as em parte a uma campanha anti-imigrantes, e o senador Robert Dole, que come�ou a rever seu tradicional apoio � abertura do mercado norte-americano, o ocupante atual da Casa Branca se ver� for�ado a deslizar em dire��o a posi��es cada vez mais demag�gicas.</s>
</p>
<p docid="mu94de05-a" nump="8" wdcount="39" nums="3">
<s docid="mu94de05-a" num="20" wdcount="13">Segundo efeito: o triunfo republicano ter� consequ�ncias pol�ticas e ideol�gicas substanciais e n�o puramente ret�ricas ou superficiais.</s>
<s docid="mu94de05-a" num="21" wdcount="10">O tr�nsito iniciado em 1938 em dire��o � conforma��o de uma maioria republicana e conservadora no Congresso consumou-se, finalmente, em 1994.</s>
<s docid="mu94de05-a" num="22" wdcount="16">Em 1938, a perda da maioria por Franklin Roosevelt e o Partido Democrata p�s fim ao New Deal; em 1948, a reconquista da maioria democrata alterou a correla��o de for�as partid�ria, mas n�o pol�tica.</s>
</p>
<p docid="mu94de05-a" nump="9" wdcount="2" nums="1">
<s docid="mu94de05-a" num="23" wdcount="2">Virada ideol�gica</s>
</p>
<p docid="mu94de05-a" nump="10" wdcount="63" nums="3">
<s docid="mu94de05-a" num="24" wdcount="29">Em 1964, a avassaladora vit�ria de Lyndon Johnson lhe garantiu uma maioria progressista no Congresso, para ratificar seus programas sociais e anti-racistas, mas esta foi ef�mera: desvaneceu-se em 1968.</s>
<s docid="mu94de05-a" num="25" wdcount="16"> Assim, entre 1932 e 1994 predominou uma maioria conservadora no Congresso, embora os democratas dominassem formalmente.</s>
<s docid="mu94de05-a" num="26" wdcount="18">Hoje a maioria ideol�gica se alinha com a partid�ria: trata-se, possivelmente, de uma mudan�a que ter� longa dura��o.</s>
</p>
<p docid="mu94de05-a" nump="11" wdcount="70" nums="2">
<s docid="mu94de05-a" num="27" wdcount="34">O v�cio profundo da democracia americana �a absten��o eleitoral do eleitorado pobre, negro e hisp�nico, e a alta participa��o dos eleitores brancos, anglo-sax�es, suburbanos e de classe m�dia a alta, acabou por se impor.</s>
<s docid="mu94de05-a" num="28" wdcount="36">A vit�ria republicana � a vit�ria de uma maioria homog�nea de uma minoria uniforme: mais de 50% dos 35%, quase todos � imagem do Sonho Americano: Bart Simpson e seu cl� ao vivo, direto das urnas.</s>
</p>
<p docid="mu94de05-a" nump="12" wdcount="71" nums="3">
<s docid="mu94de05-a" num="29" wdcount="23">Aconteceu com Clinton o mesmo que aconteceu com Carter; ambos procuraram evitar que acontecesse com eles o que acontecera com Roosevelt e Johnson.</s>
<s docid="mu94de05-a" num="30" wdcount="13">Estes dois, acredita-se, penderam demais para a esquerda; a classe m�dia os abandonou.</s>
<s docid="mu94de05-a" num="31" wdcount="35">Vem disso o fato de que os dois sulistas, Carter e Clinton, tenham pendido � direita, antes que a mesma coisa acontecesse a eles; ao faz�-lo, perderam seu eleitorado tradicional, que se refugiou na absten��o.</s>
</p>
<p docid="mu94de05-a" nump="13" wdcount="41" nums="2">
<s docid="mu94de05-a" num="32" wdcount="25">Clinton sacrificou a reforma do sistema de sa�de e conseguiu a aprova��o de uma lei anti-criminosos e o acordo de livre com�rcio com o M�xico.</s>
<s docid="mu94de05-a" num="33" wdcount="16">Os eleitores conservadores n�o o agradeceram pelo segundo �tem; os progressistas n�o o perdoaram pelo primeiro.</s>
</p>
<p docid="mu94de05-a" nump="14" wdcount="60" nums="3">
<s docid="mu94de05-a" num="34" wdcount="27">Isto significa que o vigor ou impulso dos novos dirigentes do Capit�lio � muito superior �quele que emana de um acaso, de um acidente ou uma surpresa.</s>
<s docid="mu94de05-a" num="35" wdcount="15">O portfolio de reivindica��es conservadoras n�o se limita a alguns temas tradicionais do republicanismo cl�ssico.</s>
<s docid="mu94de05-a" num="36" wdcount="18">Inclui um extremismo cultural not�vel por suas fobias �antiaborto, antiimigra��o, anti-homossexualidade, pr�-religi�o� e uma exalta��o dos  valores americanos.</s>
</p>
<p docid="mu94de05-a" nump="15" wdcount="81" nums="3">
<s docid="mu94de05-a" num="37" wdcount="33">Os dogmas n�o costumam prestar-se a negocia��es, nem mesmo no pa�s mais pragm�tico de todos, e � prov�vel que a predile��o clintoniana pela concilia��o se choque com o fervor conservador da direita crist�.</s>
<s docid="mu94de05-a" num="38" wdcount="20">Isto pode introduzir turbul�ncias preocupantes em pelo menos dois �mbitos importantes das rela��es entre a Am�rica Latina e os EUA.</s>
<s docid="mu94de05-a" num="39" wdcount="28">Deixaremos de lado, por enquanto, um terceiro tema potencialmente conflitivo: o do aborto e do controle de natalidade, e o financiamento dos programas de planejamento familiar na AL.</s>
</p>
<p docid="mu94de05-a" nump="16" wdcount="38" nums="2">
<s docid="mu94de05-a" num="40" wdcount="13">O primeiro tema espinhoso ser� um que j� conhecemos: o combate ao narcotr�fico.</s>
<s docid="mu94de05-a" num="41" wdcount="25">Os republicanos costumam atribuir a m�xima import�ncia ao assunto e muitos extremistas reaganianos da cruzada antidrogas dos anos 80 ocupam posi��es pr�ximas � nova direita.</s>
</p>
<p docid="mu94de05-a" nump="17" wdcount="57" nums="2">
<s docid="mu94de05-a" num="42" wdcount="28">� um tema intervencionista por excel�ncia, que permite colocar os problemas de forma manique�sta, condicionando todo tipo de apoios e prefer�ncias comerciais e financeiras norte-americanas a seu cumprimento.</s>
<s docid="mu94de05-a" num="43" wdcount="29">A crescente inger�ncia de Washington na luta antidrogas no interior de cada pa�s poder� se intensificar, sobretudo se os Estados da regi�o n�o declararem, eles mesmos, guerra ao tr�fico.</s>
</p>
<p docid="mu94de05-a" nump="18" wdcount="72" nums="3">
<s docid="mu94de05-a" num="44" wdcount="17">Mas a consequ�ncia mais negativa do maremoto conservador de 8 de novembro dir� respeito � quest�o migrat�ria.</s>
<s docid="mu94de05-a" num="45" wdcount="14">A nova maioria republicana em Washington tem uma agenda migrat�ria e vai lev�-la adiante.</s>
<s docid="mu94de05-a" num="46" wdcount="41">J� comprovou que o tema mexe com a classe m�dia, tanto a classe m�dia conservadora e racista que reprova a presen�a de estrangeiros quanto aquela mais tolerante e liberal, que recha�a a ilegalidade e seus efeitos nocivos para sua pr�pria sociedade.</s>
</p>
<p docid="mu94de05-a" nump="19" wdcount="36" nums="1">
<s docid="mu94de05-a" num="47" wdcount="36">Se n�o promover um equivalente federal da Proposta 187, que busca subtrair direitos educacionais e de sa�de aos trabalhadores n�o-documentados e suas fam�lias, propiciar� mudan�as na lei migrat�ria que restrinja o ingresso de migrantes sem documentos.</s>
</p>
<p docid="mu94de05-a" nump="20" wdcount="1" nums="1">
<s docid="mu94de05-a" num="48" wdcount="1">Imigrantes</s>
</p>
<p docid="mu94de05-a" nump="21" wdcount="10" nums="1">
<s docid="mu94de05-a" num="49" wdcount="10">A �poca do liberalismo migrat�rio de fato terminou nos EUA.</s>
</p>
<p docid="mu94de05-a" nump="22" wdcount="84" nums="3">
<s docid="mu94de05-a" num="50" wdcount="30">Tamb�m acabou a era durante a qual o tema migrat�rio permaneceu fora da agenda negociadora hemisf�rica, salvo contadas exce��es conjunturais: Cuba de vez em quando, as Antilhas em determinadas ocasi�es.</s>
<s docid="mu94de05-a" num="51" wdcount="33">Conv�m recordar: s�o muitos os pa�ses da Am�rica Latina que enviaram uma alta porcentagem de seus habitantes �mais de 5%, em alguns casos mais de 10%� para trabalhar e viver nos Estados Unidos.</s>
<s docid="mu94de05-a" num="52" wdcount="21">O M�xico, quase toda a Am�rica Central, boa parte do Caribe, Col�mbia, Equador e Peru s�o na��es fortemente expulsadoras de migrantes.</s>
</p>
<p docid="mu94de05-a" nump="23" wdcount="86" nums="3">
<s docid="mu94de05-a" num="53" wdcount="27">Para todos esses pa�ses, e para seus respectivos governos, o fechamento norte-americano em mat�ria de imigra��o vai obrigar, cedo ou tarde, a uma negocia��o delicada e complexa.</s>
<s docid="mu94de05-a" num="54" wdcount="19">Os inevit�veis termos desta negocia��o j� anunciam o dif�cil dilema que ir� se colocar: legaliza��o ampliada contra regula��o compartilhada.</s>
<s docid="mu94de05-a" num="55" wdcount="14">As alternativas ser�o angustiantes para todos, e para o M�xico principalmente, devido � fronteira.</s>
<s docid="mu94de05-a" num="56" wdcount="13">Mas nenhum pa�s permanecer� � margem da virada � direita nos Estados Unidos.</s>
<s docid="mu94de05-a" num="57" wdcount="13">Para a Am�rica Latina, como sempre, muita coisa � decidida fora de casa.</s>
</p>





</TEXT>

</DOC>