<DOC>

<FILEID>
<s docid="op94ab24-a" num="1" wdcount="1">op94ab24-a</s>
</FILEID>

<HEAD>
<s docid="op94ab24-a" num="2" wdcount="2"> SE��O:  EDITORIAL </s>
<s docid="op94ab24-a" num="3" wdcount="3"> Pagando a conta </s>
</HEAD>

<CATEGORY>
<s docid="op94ab24-a" num="4" wdcount="1"> Opiniao </s>
</CATEGORY>



<TEXT>


<p docid="op94ab24-a" nump="1" wdcount="68" nums="3">
<s docid="op94ab24-a" num="5" wdcount="46">� parte qualquer l�gica eleitoral que possa ter, o acordo entre PFL e PSDB, desde que come�ou a ser cogitado, acenava com toda sorte de dilemas e dificuldades em fun��o da grande disparidade entre as duas legendas em termos de ideologia, hist�ria partid�ria e pr�tica pol�tica.</s>
<s docid="op94ab24-a" num="6" wdcount="22">T�o d�spares que boa parte dos membros de uma apoiou a ditadura contra a qual lutou a maioria dos membros da outra.</s>
</p>
<p docid="op94ab24-a" nump="2" wdcount="58" nums="1">
<s docid="op94ab24-a" num="7" wdcount="58">Pois al�m das amea�as de rebeli�o que se disseminaram de imediato em diversos setores do PSDB, a estranha alian�a j� provocou um novo e embara�oso problema a partir do favoritismo que vinha sendo dado ao nome do deputado Lu�s Eduardo Magalh�es (PFL-BA) entre os cotados para compor, como candidato a vice, a chapa presidencial de Fernando Henrique Cardoso.</s>
</p>
<p docid="op94ab24-a" nump="3" wdcount="72" nums="3">
<s docid="op94ab24-a" num="8" wdcount="54">Lu�s Eduardo de fato j� tinha a caracter�stica peculiar de, como filho de Ant�nio Carlos Magalh�es, representar quase que um emblema do PFL �trazendo, ao lado de um vigoroso apoio do governador da Bahia, a lembran�a presente daquilo que esse partido significa em termos de pr�tica, de hist�ria e de posi��es pol�ticas no pa�s.</s>
<s docid="op94ab24-a" num="9" wdcount="3">N�o s� isso.</s>
<s docid="op94ab24-a" num="10" wdcount="15">Como foi lembrado nos �ltimos dias, Lu�s Eduardo votou contra o impeachment de Fernando Collor.</s>
</p>
<p docid="op94ab24-a" nump="10" wdcount="60" nums="2">
<s docid="op94ab24-a" num="11" wdcount="26">Ora, o PSDB sempre se pretendeu um partido preocupado com a �tica �supostamente, sua cria��o como dissid�ncia do PMDB deveu-se em boa parte a essa quest�o.</s>
<s docid="op94ab24-a" num="12" wdcount="34">Assim, se o pacto com uma legenda vinculada �s tradi��es mais deplor�veis da vida p�blica brasileira j� � dif�cil de justificar, a aceita��o de um candidato a vice que defendeu Collor parece quase imposs�vel.</s>
</p>
<p docid="op94ab24-a" nump="12" wdcount="39" nums="2">
<s docid="op94ab24-a" num="13" wdcount="38">Expresso de forma inequ�voca num dos maiores movimentos c�vicos que este pa�s j� viu �e que contou com a ades�o integral do PSDB�, o apoio da popula��o ao afastamento do chefe da Dinda e sua camarilha foi esmagador.</s>
<s docid="op94ab24-a" num="14" wdcount="29">Nessas circunst�ncias, o voto de Lu�s Eduardo contra o impeachment revela um desd�m para com a opini�o do eleitorado que atenta contra as responsabilidades e deveres da representa��o popular.</s>
</p>
<p docid="op94ab24-a" nump="13" wdcount="29" nums="1">
<s docid="op94ab24-a" num="15" wdcount="29">A cota��o do filho de ACM como postulante a vice decaiu sensivelmente nos �ltimos dias, mas mesmo que seu nome seja definitivamente afastado o problema continua longe de resolvido.</s>
</p>
<p docid="op94ab24-a" nump="14" wdcount="75" nums="2">
<s docid="op94ab24-a" num="16" wdcount="29">O fato � que o PSDB chegou a considerar seriamente o nome do pefelista baiano para integrar sua chapa, sabendo perfeitamente como ele havia votado na decis�o do impeachment.</s>
<s docid="op94ab24-a" num="17" wdcount="46">Assim, o seu poss�vel recuo agora pareceria dever-se muito mais ao temor do efeito eleitoral de uma divulga��o do voto pr�-Collor de Lu�s Eduardo do que a qualquer considera��o de ordem �tica quanto a esse mesmo voto �atitude eleitoreira do tipo que o PSDB costumava condenar.</s>
</p>
<p docid="op94ab24-a" nump="15" wdcount="67" nums="2">
<s docid="op94ab24-a" num="18" wdcount="19">Outra dificuldade que resta refere-se � escolha de um novo nome do PFL para vice na chapa de FHC.</s>
<s docid="op94ab24-a" num="19" wdcount="48">Com o filho de ACM perdendo a dianteira, ressurge a dif�cil tarefa de encontrar um nome que ao mesmo tempo satisfa�a a c�pula pefelista, traga votos no Nordeste (ao fim e ao cabo, o maior atrativo do PFL) e n�o cause uma rebeli�o ainda maior entre os tucanos.</s>
</p>
<p docid="op94ab24-a" nump="16" wdcount="34" nums="2">
<s docid="op94ab24-a" num="20" wdcount="23">S�o problemas, de todo modo, que n�o devem surpreender ningu�m �muito menos o PSDB� e que dever�o continuar a aflorar nos pr�ximos meses.</s>
<s docid="op94ab24-a" num="21" wdcount="11">Afinal, n�o se pode aliar antigos inimigos e ant�podas pol�ticos impunemente.</s>
</p>


</TEXT>

</DOC>