<DOC>

<FILEID>
<s docid="ce94ab24-e" num="1" wdcount="1">ce94ab24-e</s>
</FILEID>

<HEAD>
<s docid="ce94ab24-e" num="2" wdcount="10"> Pa�s ensaia a democracia entre o c�u e o inferno </s>
<s docid="ce94ab24-e" num="3" wdcount="16"> 'O processo de transi��o envolveo mais substancial realinhamentode poder pol�tico, militar, social eecon�mico jamais completado' </s>
<s docid="ce94ab24-e" num="4" wdcount="14"> Marginalizada durante s�culos, amaioria negra dificilmente ter�paci�ncia para esperar muitotempo por uma vida melhor </s>
</HEAD>


<CATEGORY>
<s docid="ce94ab24-e" num="5" wdcount="1"> Especial </s>
</CATEGORY>



<TEXT>


<p docid="ce94ab24-e" nump="1" wdcount="33" nums="1">
<s docid="ce94ab24-e" num="6" wdcount="33">Ao primeiro minuto da pr�xima quarta-feira as nove capitais provinciais da �frica do Sul ouvir�o pela primeira vez, ao menos oficialmente, os sons de "Nkosi Sikeleli Afrika" ("Deus Aben�oe a �frica").</s>
</p>
<p docid="ce94ab24-e" nump="2" wdcount="29" nums="1">
<s docid="ce94ab24-e" num="7" wdcount="29">� o novo hino nacional oficial, depois de ter sido, anos a fio, o hino clandestinamente cantado pela maioria negra, submetida ao apartheid, o nefando regime de segrega��o racial.</s>
</p>
<p docid="ce94ab24-e" nump="3" wdcount="22" nums="1">
<s docid="ce94ab24-e" num="8" wdcount="22">Bem que a �frica do Sul vai necessitar das ben��os dos deuses depois das elei��es marcadas para os dias 26 a 28.</s>
</p>
<p docid="ce94ab24-e" nump="4" wdcount="44" nums="1">
<s docid="ce94ab24-e" num="9" wdcount="44">Em seus 1,22 milh�o de quil�metros quadrados, v�o ensaiar uma dif�cil conviv�ncia 40,4 milh�es de pessoas, divididas entre 5,1 milh�es de brancos, 3,4 milh�es de mesti�os e 30,7 milh�es de negros, por sua vez, subdividos em 11 etnias diferentes.</s>
</p>
<p docid="ce94ab24-e" nump="5" wdcount="34" nums="2">
<s docid="ce94ab24-e" num="10" wdcount="22">Desde que, em 1652, a Companhia Holandesa das �ndias Orientais estabeleceu sua primeira col�nia na costa sul-africana, brancos e negros amaram odiar-se.</s>
<s docid="ce94ab24-e" num="11" wdcount="12">Antes e depois, negros de uma tribo matavam os de outra.</s>
</p>
<p docid="ce94ab24-e" nump="6" wdcount="25" nums="1">
<s docid="ce94ab24-e" num="12" wdcount="25">Em 1990, depois de 338 anos de cruentos conflitos, seus l�deres decidiram trocar os fuzis pelo di�logo, em busca de um fim negociado do apartheid.</s>
</p>
<p docid="ce94ab24-e" nump="7" wdcount="18" nums="1">
<s docid="ce94ab24-e" num="13" wdcount="18">O processo reduzira � mis�ria uma majorit�ria fatia da popula��o negra e conduzira ao fausto a elite branca.</s>>
</p>
<p docid="ce94ab24-e" nump="8" wdcount="23" nums="1">
<s docid="ce94ab24-e" num="14" wdcount="23">As elei��es desta semana s�o o ponto de chegada dessa negocia��o, um desses raros momentos que de fato merecem a qualifica��o de hist�ricos.</s>
</p>
<p docid="ce94ab24-e" nump="9" wdcount="12" nums="1">
<s docid="ce94ab24-e" num="15" wdcount="12">At� banqueiros, habitualmente frios, recorrem � ret�rica incandescente para avaliar a situa��o.</s>
</p>
<p docid="ce94ab24-e" nump="10" wdcount="46" nums="1">
<s docid="ce94ab24-e" num="16" wdcount="46">"O processo de transi��o em andamento envolve, talvez, o mais substancial realinhamento de poder pol�tico, militar, social e econ�mico jamais completado em uma mesa de negocia��o, em vez de no campo de batalha, incluindo o Oriente M�dio, a Europa Oriental e a antiga Uni�o Sovi�tica".</s>
</p>
<p docid="ce94ab24-e" nump="11" wdcount="13" nums="1">
<s docid="ce94ab24-e" num="17" wdcount="13">A avalia��o foi publicada em folheto do Salomon Brothers, banco internacional de investimento.</s>
</p>
<p docid="ce94ab24-e" nump="12" wdcount="38" nums="2">
<s docid="ce94ab24-e" num="18" wdcount="12">Tarefa t�o cicl�pica n�o se limitou, no entanto, � mesa de negocia��o.</s>
<s docid="ce94ab24-e" num="19" wdcount="26">Por indefinidos campos de batalha ficaram, nesses quatro anos de di�logo, os corpos de 13.724 pessoas, conforme o mais recente c�mputo da Comiss�o de Direitos Humanos.</s>
</p>
<p docid="ce94ab24-e" nump="13" wdcount="25" nums="1">
<s docid="ce94ab24-e" num="20" wdcount="25">Quase dez mortos por dia, s� pela viol�ncia pol�tica, sem contar a elevada cota da criminalidade comum (16 mil assassinatos apenas no ano de 1992).</s>
</p>
<p docid="ce94ab24-e" nump="14" wdcount="25" nums="1">
<s docid="ce94ab24-e" num="21" wdcount="25">Seria otimismo desmesurado supor que a realiza��o da primeira elei��o multirracial, por mais hist�rica que seja, basta para p�r fim � viol�ncia, pol�tica ou comum.</s>
</p>
<p docid="ce94ab24-e" nump="15" wdcount="23" nums="1">
<s docid="ce94ab24-e" num="22" wdcount="23">Os conflitos tribais que ensaguentam boa parte do mapa africano e as guerras �tnicas em plena Europa assombram uma �frica do Sul democr�tica.</s>
</p>
<p docid="ce94ab24-e" nump="16" wdcount="46" nums="1">
<s docid="ce94ab24-e" num="23" wdcount="46">"As experi�ncias da �frica independente, da Iugosl�via e da ex- URSS demonstraram claramente como � dif�cil substituir identidades �tnicas individuais por um compromisso com um �nico e abrangente nacionalismo", admite Zola Skweyiya, advogado do partido Conselho Nacional Africano (CNA), o mais prov�vel ganhador da elei��o.</s>
</p>
<p docid="ce94ab24-e" nump="17" wdcount="33" nums="2">
<s docid="ce94ab24-e" num="24" wdcount="7">O per�odo pr�-eleitoral d� raz�o a ele.</s>
<s docid="ce94ab24-e" num="25" wdcount="26">Pelo menos uma parcela dos brancos come�a a se concentrar em �reas do Transvaal e do Estado Livre de Orange, na zona centro-oriental do pa�s.</s>
</p>
<p docid="ce94ab24-e" nump="18" wdcount="15" nums="1">
<s docid="ce94ab24-e" num="26" wdcount="15">� um ensaio para se criar um "Volkstaat" (p�tria para os afrik�ners, os brancos sul-africanos).</s>
</p>
<p docid="ce94ab24-e" nump="19" wdcount="25" nums="1">
<s docid="ce94ab24-e" num="27" wdcount="25">Tamb�m a fatia dos zulus, a maior etnia negra (8,3 milh�es), pretende fazer do KwaZulu (literalmente "o lugar dos zulus") um pa�s independente.</s>
</p>
<p docid="ce94ab24-e" nump="20" wdcount="30" nums="2">
<s docid="ce94ab24-e" num="28" wdcount="10">� essa press�o de fundo �tnico, soma-se a press�o social.</s>
<s docid="ce94ab24-e" num="29" wdcount="20">Marginalizada durante s�culos, a maioria negra dificilmente ter� paci�ncia para esperar muito tempo para ter a uma vida melhor.</s>
</p>
<p docid="ce94ab24-e" nump="21" wdcount="42" nums="2">
<s docid="ce94ab24-e" num="30" wdcount="22">Na edi��o que foi anteontem �s bancas, o seman�rio "The Weekly Mail and Guardian" publica hist�ria na popular tira "Madame e Eva".</s>
<s docid="ce94ab24-e" num="31" wdcount="20">Um casal de negros apresenta-se na casa de uma senhora branca, afirmando estar "ca�ando casa para depois da elei��o".</s>
</p>
<p docid="ce94ab24-e" nump="22" wdcount="30" nums="2">
<s docid="ce94ab24-e" num="32" wdcount="20">O casal encanta-se com o lustre de cristal, quando a dona avisa: "Sinto, mas a casa n�o est� � venda".</s>
<s docid="ce94ab24-e" num="33" wdcount="10">Os negros retrucam: "E quem disse algo sobre comprar?"</s>
</p>
<p docid="ce94ab24-e" nump="23" wdcount="22" nums="1">
<s docid="ce94ab24-e" num="34" wdcount="22"> O alto n�vel de expectativas da maioria negra com a troca de guarda no pal�cio governamental � admitido pela c�pula do CNA.</s>
</p>
<p docid="ce94ab24-e" nump="24" wdcount="21" nums="1">
<s docid="ce94ab24-e" num="35" wdcount="21">"N�o h� crime em as pessoas desejarem coisas que lhes d�em uma vida melhor", afirma Cyril Ramaphosa, secret�rio-geral do partido.</s>
</p>
<p docid="ce94ab24-e" nump="25" wdcount="33" nums="2">
<s docid="ce94ab24-e" num="36" wdcount="10">De fato n�o h� crime em querer melhorar de vida.</s>
<s docid="ce94ab24-e" num="37" wdcount="23">Mas tampouco h� resposta para a pergunta que o Ramaphosa faz a respeito do futuro imediato: "Seremos capazes de satisfazer tais expectativas?"</s>
</p>
<p docid="ce94ab24-e" nump="26" wdcount="61" nums="1">
<s docid="ce94ab24-e" num="38" wdcount="61">Pelo menos no papel, o CNA promete muito: criar, em dez anos, 2,5 milh�es de empregos, por meio de um programa nacional de obras p�blicas; construir 1 milh�o de casas, em cinco anos; no mesmo prazo, colocar eletricidade em 2,5 milh�es de resid�ncias; prover dez anos de educa��o gr�tis e de qualidade para todos, dentro de meros 365 dias.</s>
</p>
<p docid="ce94ab24-e" nump="27" wdcount="38" nums="2">
<s docid="ce94ab24-e" num="39" wdcount="28">Se conseguir tudo isso e, ainda, controlar os previs�veis conflitos �tnicos, a �frica do Sul nem precisar� cantar "Nkosi Sikelele Afrika", porque j� ter� sido aben�oada pelos deuses.</s>
<s docid="ce94ab24-e" num="40" wdcount="10">Se n�o, ser� apenas mais um inferno africano.(CR)</s>
</p>

</TEXT>

</DOC>