<DOC>

<FILEID>
<s docid="in96jl02-a" num="1" wdcount="1">in96jl02-a</s>
</FILEID>

<HEAD>
<s docid="in96jl02-a" num="2" wdcount="6"> Yeltsin est� vivo mas parece doente </s>
<s docid="in96jl02-a" num="3" wdcount="19"> Na antev�spera do segundo turno, presidente russo deixa muitas d�vidas sobre seu estado de sa�de e capacidade para governar
 </s>
</HEAD>

<CATEGORY>
<s docid="in96jl02-a" num="4" wdcount="1"> Internacional </s>
</CATEGORY>



<TEXT>


<p docid="in96jl02-a" nump="1" wdcount="96" nums="6">
<s docid="in96jl02-a" num="5" wdcount="24">MOSCOU - Ap�s um sumi�o de quatro dias, o presidente da R�ssia, Boris Yeltsin, reapareceu ontem para um pronunciamento de dois minutos pela televis�o.</s>
<s docid="in96jl02-a" num="6" wdcount="32">A mensagem - que leu no teleprompter - era um pedido aos eleitores para que n�o deixem de votar amanh� no segundo turno que ele e o comunista Guenadi Ziuganov v�o disputar.</s>
<s docid="in96jl02-a" num="7" wdcount="11">"Sei exatamente o que fazer, tenho for�a, desejo e decis�o.</s>
<s docid="in96jl02-a" num="8" wdcount="14">O que preciso agora � de seu apoio", afirmou Yeltsin com a voz cansada.</s>
<s docid="in96jl02-a" num="9" wdcount="7">"Voc�s n�o devem ficar em casa.</s>
<s docid="in96jl02-a" num="10" wdcount="8">N�o votar � uma op��o contra a R�ssia."</s>
</p>
<p docid="in96jl02-a" nump="2" wdcount="67" nums="2">
<s docid="in96jl02-a" num="11" wdcount="25">Praticamente sem se mover, mexendo apenas os l�bios, Yeltsin era uma sombra do candidato que h� semanas dan�ou rock durante um showm�cio para jovens eleitores.</s>
<s docid="in96jl02-a" num="12" wdcount="42">Se ontem tranquilizou o eleitorado (j� se especulava que sofrera outro ataque card�aco e morrera) mostrando que est� vivo, o presidente, que busca a reelei��o, deixou muitas d�vidas sobre sua sa�de e sua capacidade para governar a R�ssia por mais quatro anos.</s>
</p>
<p docid="in96jl02-a" nump="3" wdcount="82" nums="3">
<s docid="in96jl02-a" num="13" wdcount="32">Gripe - A explica��o oficial para os quatro dias de sumi�o, quando deixou de comparecer a cerim�nias oficiais e abandonou a campanha, foi dada pelo primeiro-ministro Viktor Chernomirdin: Yeltsin estava com laringite.</s>
<s docid="in96jl02-a" num="14" wdcount="18">Mas Yeltsin n�o parecia rouco e, rapidamente, a vers�o oficial passou a ser que ele est� muito gripado.</s>
<s docid="in96jl02-a" num="15" wdcount="32">Foi o bastante para a imprensa russa especular sobre o ocorrido nas d�cadas de 70 e 80 com Leonid Brejnev, Yuri Andropov e Konstantin Chernenko, que permaneceram no Kremlin, apesar de incapacitados.
</s>
</p>
<p docid="in96jl02-a" nump="4" wdcount="62" nums="6">
<s docid="in96jl02-a" num="16" wdcount="11">Quem tamb�m se lembrou dos velhos tempos foi o advers�rio Ziuganov.</s>
<s docid="in96jl02-a" num="17" wdcount="9">"N�o vemos o Senhor Yeltsin h� quatro dias".</s>
<s docid="in96jl02-a" num="18" wdcount="13">Assim, o comunista come�ou um pronunciamento pela televis�o, antes do reaparecimento do presidente.</s>
<s docid="in96jl02-a" num="19" wdcount="14">"Quero ver o laudo oficial sobre o estado de sa�de do Senhor Yeltsin.</s>
<s docid="in96jl02-a" num="20" wdcount="7">Gostaria de saber o que est� acontecendo.</s>
<s docid="in96jl02-a" num="21" wdcount="8">Por que todos os compromissos oficiais foram cancelados?"</s>
</p>
<p docid="in96jl02-a" nump="5" wdcount="41" nums="2">
<s docid="in96jl02-a" num="22" wdcount="20">No Parlamento, Stanislav Govorukhin, deputado nacionalista partid�rio de Ziuganov, disse que Yeltsin "parecia um zumbi" durante sua fala pela televis�o.</s>
<s docid="in96jl02-a" num="23" wdcount="21">"Sumiu durante dias e o que nos mostraram hoje (ontem) foi uma m�mia pintada e est�o pedindo que votemos nela."</s>
</p>
<p docid="in96jl02-a" nump="6" wdcount="63" nums="3">
<s docid="in96jl02-a" num="24" wdcount="25">Govorukhin exigiu do Kremlin o adiamento das elei��es por duas, tr�s semanas ou at� um m�s para dar a Yeltsin a chance de se recuperar.</s>
<s docid="in96jl02-a" num="25" wdcount="25">Em sua opini�o, os assessores de Yeltsin est�o se comportando como gera��es de funcion�rios do Kremlin que escondem a verdade at� que seja tarde demais.</s>
<s docid="in96jl02-a" num="26" wdcount="13">"Voc�s se lembram que quando Stalin morreu, s� anunciaram tr�s dias depois."</s>
</p>
<p docid="in96jl02-a" nump="7" wdcount="77" nums="4">
<s docid="in96jl02-a" num="27" wdcount="21">Leis - Com Yeltsin parecendo estar na ante-sala da UTI, n�o se sabe o que pode acontecer de hoje para amanh�.</s>
<s docid="in96jl02-a" num="28" wdcount="19">A Constitui��o diz que se o presidente ficar doente, o primeiro-ministro assume e convoca nova elei��o em tr�s meses.</s>
<s docid="in96jl02-a" num="29" wdcount="26">Mas a lei sobre as elei��es presidenciais diz que, se um dos candidatos ao segundo turno se retirar, ser� substitu�do pelo terceiro colocado no primeiro turno.</s>
<s docid="in96jl02-a" num="30" wdcount="11">No caso o general Alexander Lebed, atual respons�vel pela seguran�a nacional.</s>
</p>
<p docid="in96jl02-a" nump="8" wdcount="106" nums="5">
<s docid="in96jl02-a" num="31" wdcount="15">Aos 65 anos, Boris Yeltsin j� viveu mais do que a m�dia de seus compatriotas.</s>
<s docid="in96jl02-a" num="32" wdcount="30">Card�aco desde 1987, o presidente russo nos �ltimos 18 meses esteve internado pelo menos tr�s vezes: em dezembro de 1994, no fim de julho de 1995 e em outubro passado.</s>
<s docid="in96jl02-a" num="33" wdcount="17">Da primeira vez, Yeltsin passou duas semanas no hospital, oficialmente para extra��o de tumor benigno no nariz.</s>
<s docid="in96jl02-a" num="34" wdcount="33">Mas em julho de 1995, ap�s nova interna��o, o Kremlin foi obrigado a informar que ele sofrera uma isquemia do mioc�rdio (falta de oxig�nio no m�sculo card�aco) e ficaria 15 dias se recuperando.</s>
<s docid="in96jl02-a" num="35" wdcount="11">Tr�s meses depois, novo ataque card�aco e 60 dias no hospital.</s>
</p>
<p docid="in96jl02-a" nump="8" wdcount="39" nums="2">
<s docid="in96jl02-a" num="36" wdcount="19">Os problemas card�acos do presidente russo datam de novembro de 1987 e s�o agravados pela cirrose, causada pela bebida.</s>
<s docid="in96jl02-a" num="37" wdcount="20">Al�m disso, Yeltsin sofre de depress�o e ins�nia e tem problemas de coluna, desde um acidente em 1990 na Espanha.</s>
</p>

</TEXT>

</DOC>