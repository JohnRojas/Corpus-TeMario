<DOC>

<FILEID>
<s docid="in96jl01-a" num="1" wdcount="1">in96jl01-a</s>
</FILEID>

<HEAD>
<s docid="in96jl01-a" num="2" wdcount="4"> Oposi��o conquista Buenos Aires </s>
<s docid="in96jl01-a" num="3" wdcount="15"> Candidato de Menem fica em terceiro lugar na primeira elei��o para prefeito da capital argentina. </s>
</HEAD>

<AUTHOR>
<s docid="in96jl01-a" num="4" wdcount="9"> Eleito � do partido de Alfons�n, MARCIA CARMO (Correspondente) </s>
</AUTHOR>

<CATEGORY>
<s docid="in96jl01-a" num="5" wdcount="1"> Internacional </s>
</CATEGORY>



<TEXT>


<p docid="in96jl01-a" nump="1" wdcount="67" nums="2">
<s docid="in96jl01-a" num="6" wdcount="33">BUENOS AIRES - Na sua primeira elei��o direta, a capital do tango elegeu ontem o senador da oposicionista Uni�o C�vica Radical (UCR) Fernando De La R�a, de 58 anos, para prefeito da cidade.</s>
<s docid="in96jl01-a" num="7" wdcount="34">O resultado fortaleceu a esquerda e promete agitar a elei��o presidencial que ser� realizada em 1999 e at� agora s� apresentava como op��o certa o presidente Carlos Menem, que est� em seu segundo mandato.</s>
</p>
<p docid="in96jl01-a" nump="2" wdcount="84" nums="2">
<s docid="in96jl01-a" num="8" wdcount="56">Meia hora depois de anunciado os resultados das pesquisas boca-de-urna, �s 18h, que lhe deram cerca de 40% dos votos, Fernando De La R�a, advogado, casado, pai de tr�s filhos, anunciou: "Foi a vit�ria da democracia." O presidente do seu partido, Rodolfo Terragno, foi mais duro: "� o in�cio de uma nova etapa para o pa�s.</s>
<s docid="in96jl01-a" num="9" wdcount="28">O fim da corrup��o e do desemprego." � noite, apesar do frio, os radicais, que estavam politicamente apagados, reuniram uma multid�o num com�cio improvisado no centro da cidade.
</s>
</p>
<p docid="in96jl01-a" nump="3" wdcount="41" nums="2">
<s docid="in96jl01-a" num="10" wdcount="20">Segundo as pesquisas de boca-de-urna, o segundo colocado foi Norberto de la Porta, da tamb�m oposicionista Frente Pa�s Solid�rio (Frepaso).</s>
<s docid="in96jl01-a" num="11" wdcount="21">O atual prefeito bi�nico Jorge Dom�ngues, do governista Partido Justicialista (peronista), ficou em terceiro lugar, com cerca de 15% dos votos.</s>
</p>
<p docid="in96jl01-a" nump="4" wdcount="131" nums="6">
<s docid="in96jl01-a" num="12" wdcount="25">No dia do seu 66� anivers�rio, ao reconhecer a derrota de seu candidato, o presidente Menem disse que a elei��o foi um "resultado da democracia".</s>
<s docid="in96jl01-a" num="13" wdcount="13">Al�m da prefeitura, os governistas perderam ainda na escolha para os chamados constituintes.</s>
<s docid="in96jl01-a" num="14" wdcount="29">Desemprego - Nessa corrida, a l�der foi a senadora Graciela Fern�ndez Meijidi, da Frente Pa�s Solid�rio, que com outros parlamentares e representantes da sociedade redigir� a Constitui��o da cidade.</s>
<s docid="in96jl01-a" num="15" wdcount="7">O vice-presidente Carlos Ruckaulf chegou em terceiro.</s>
<s docid="in96jl01-a" num="16" wdcount="22">"Vamos fazer um estatuto preocupado com a situa��o dos cidad�os argentinos", disse Graciela, uma das principais vozes de oposi��o a Menem.</s>
<s docid="in96jl01-a" num="17" wdcount="35">A senadora lembrou que a Frepaso, apesar de s� ter cinco anos, chegou em segundo lugar nas elei��es presidenciais do ano passado, a elegeu senadora no fim do ano passado e agora a sagrou constituinte.</s>
</p>
<p docid="in96jl01-a" nump="5" wdcount="110" nums="4">
<s docid="in96jl01-a" num="18" wdcount="11">Ontem, existiam v�rias justificativas para a vit�ria da oposi��o nas urnas.</s>
<s docid="in96jl01-a" num="19" wdcount="44">Al�m do tradicional voto de protesto na capital federal, ela foi atribu�da ao aumento do �ndice de desemprego (que atinge agora 2,2 milh�es de trabalhadores, ou 17,1% da popula��o economicamente ativa) e ao prov�vel reajuste das tarifas telef�nicas e outros servi�os p�blicos.</s>
<s docid="in96jl01-a" num="20" wdcount="30">O governador da prov�ncia de Buenos Aires, Eduardo Duhalde, que � do mesmo partido de Menem, mas j� est� de olho na corrida presidencial, comparou os desempregados argentinos a "p�rias".</s>
<s docid="in96jl01-a" num="21" wdcount="25">Ele reconheceu que o desemprego � um problema mundial, mas disse que na Europa, por exemplo, existem programas sociais de compensa��o � falta de trabalho.</s>
</p>
<p docid="in96jl01-a" nump="6" wdcount="60" nums="2">
<s docid="in96jl01-a" num="22" wdcount="47">A elei��o de Fernando De La R�a, senador por tr�s mandatos, sendo que um interrompido durante a ditadura militar, foi poss�vel gra�as � reforma constitucional realizada h� dois anos, que deu � capital argentina o direito de eleger seus governantes, al�m de permitir a reelei��o do presidente.</s>
<s docid="in96jl01-a" num="23" wdcount="13">"Agora � trabalhar, trabalhar e trabalhar", afirmou o novo prefeito da cidade.</s>
</p>
<p docid="in96jl01-a" nump="7" wdcount="75" nums="3">
<s docid="in96jl01-a" num="24" wdcount="7">"� apenas o ponto de partida.</s>
<s docid="in96jl01-a" num="25" wdcount="11">E para acabar com as injusti�as precisaremos do apoio da sociedade."</s>
<s docid="in96jl01-a" num="26" wdcount="57">A vit�ria de De la R�a significa, tamb�m, o renascimento da Uni�o C�vica Radical, o mais antigo partido da Argentina, que entrou em crise devido ao final turbulento do governo de Ra�l Alfons�n, em 1989, quando a hiperinfla��o e as amea�as de golpe militar levaram o ent�o presidente a antecipar em dois meses a posse de Menem.</s>
</p>


</TEXT>

</DOC>